package com.ringme.automationtest

import org.hamcrest.MatcherAssert
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ValidatorTest{
    @Test
    fun whenInputIsValid(){
        val amount = 100
        val desc = "Hello A"
        val result = Validator.inputAmount(amount, desc)
        Assert.assertEquals(true, result)
    }
    @Test
    fun whenInputIsInValid(){
        val amount = 100
        val desc = ""
        val result = Validator.inputAmount(amount, desc)
        Assert.assertEquals(false, result)
    }
}