package com.ringme.automationtest

import android.app.Application
import com.ringme.automationtest.data.SendDatabase

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        SendDatabase.getInstance(this)
    }
}