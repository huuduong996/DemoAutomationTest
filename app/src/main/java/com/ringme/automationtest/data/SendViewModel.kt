package com.ringme.automationtest.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class SendViewModel(val daoSource: SpendsTrackerDataSource):ViewModel() {

    val list = MutableLiveData<List<Send>>()
    val listLiveData : LiveData<List<Send>>  = list

    fun addSpend(amount: Int, description: String) = viewModelScope.launch {
        daoSource.addSend(Send(amount, description))
    }

    fun getLast20Spends() = viewModelScope.launch {
        list.value = daoSource.getAllLast20()
    }
}