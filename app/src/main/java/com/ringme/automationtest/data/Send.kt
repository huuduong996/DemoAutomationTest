package com.ringme.automationtest.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "spends")
data class Send(val amount: Int, val desc: String) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}