package com.ringme.automationtest.data

class SpendsTrackerDataSource(val dao: SedDao) {

    suspend fun addSend(model: Send) = dao.addSend(model)

    suspend fun getAllLast20() =  dao.getLast20Send()
}