package com.ringme.automationtest.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Send::class], version = 1)
abstract class SendDatabase: RoomDatabase() {

    abstract  fun getSendDao(): SedDao

    companion object{
        private const val DB_NAME = "SpendsKA"
        private var INSTANCE: SendDatabase? = null

        open fun getInstance(context: Context?): SendDatabase? {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context!!, SendDatabase::class.java, "app_database")
                    .build()
            }
            return INSTANCE
        }
    }


}