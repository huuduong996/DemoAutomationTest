package com.ringme.automationtest.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface SedDao {
    @Insert
    suspend fun addSend(model: Send)

    @Query("SELECT * FROM spends LIMIT 20")
    fun getLast20Send():List<Send>
}