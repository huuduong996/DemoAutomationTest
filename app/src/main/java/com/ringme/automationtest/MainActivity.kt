package com.ringme.automationtest

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton

class MainActivity : AppCompatActivity() {
    public var PICK_REQUEST = 123
    lateinit var btnContact: AppCompatButton
    lateinit var btnLogin: AppCompatButton
    lateinit var btnStart: AppCompatButton

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnContact = findViewById(R.id.btn_contact)
        btnLogin = findViewById(R.id.btn_login)
        btnStart = findViewById(R.id.btn_start)
        btnStart.setOnClickListener {
            startNewActivity()
        }
        btnLogin.setOnClickListener {
            startActivity(Intent(this@MainActivity, SecondActivity::class.java))
        }
        btnContact.setOnClickListener {
            pickContact(it)
        }
    }

    fun pickContact(v: View) {
        val i = Intent(
            Intent.ACTION_PICK,
            ContactsContract.Contacts.CONTENT_URI
        )
        startActivityForResult(i, PICK_REQUEST)
    }

    private fun startNewActivity() {
        val intent = Intent(applicationContext, SecondActivity::class.java)
        intent.putExtra("URL", "haobk4dev.wordpress.com")
        startActivity(intent)
    }
}