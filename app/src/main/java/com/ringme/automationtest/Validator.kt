package com.ringme.automationtest

object Validator {
    fun inputAmount(amount: Int, desc: String): Boolean{
        return !(amount <=0 || desc.isEmpty())
    }
}