package com.ringme.automationtest

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.ringme.automationtest.data.SedDao
import com.ringme.automationtest.data.Send
import com.ringme.automationtest.data.SendDatabase
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class SendDatabaseTest : TestCase(){
    private var db: SendDatabase? = null
    private var dao: SedDao? = null

    @Before
    public override fun setUp(){
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, SendDatabase::class.java).build()
        dao = db!!.getSendDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDB(){
        db?.close()
    }

    @Test
    fun writeAndReadSpend() = runBlocking {
        val spend = Send( 100, "for Bread")
        dao?.addSend(spend)
        val spends : ArrayList<Send> = ArrayList()
        spends.addAll(dao!!.getLast20Send())
        assertEquals(true, spends.contains(spend))
    }
}