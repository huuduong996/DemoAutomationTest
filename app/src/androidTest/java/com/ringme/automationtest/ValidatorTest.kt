package com.ringme.automationtest

import org.junit.Assert
import org.junit.Assert.*
import org.junit.Test

class ValidatorTest{
    @Test
    fun whenInputIsValid(){
        val amount = 100
        val desc = "Hello A"
        val result = Validator.inputAmount(amount, desc)
        Assert.assertEquals(true, result)
    }
    @Test
    fun whenInputIsInValidDesc(){
        val amount = 100
        val desc = ""
        val result = Validator.inputAmount(amount, desc)
        Assert.assertEquals(false, result)
    }

    @Test
    fun whenInputIsInValidAmount(){
        val amount = -1
        val desc = "A bc"
        val result = Validator.inputAmount(amount, desc)
        Assert.assertEquals(false, result)
    }
}