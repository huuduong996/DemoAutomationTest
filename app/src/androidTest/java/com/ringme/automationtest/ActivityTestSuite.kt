package com.ringme.automationtest

import com.ringme.automationtest.data.SendDatabase
import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(
    MainActivityTest::class,
    SecondActivityTest::class,
    FragmentAddTest::class
)
class ActivityTestSuite {
}