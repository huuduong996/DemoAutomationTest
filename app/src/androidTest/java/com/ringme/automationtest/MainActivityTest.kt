package com.ringme.automationtest

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.provider.ContactsContract
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasAction
import androidx.test.espresso.intent.matcher.IntentMatchers.hasData
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.intent.matcher.IntentMatchers.toPackage
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.hamcrest.Matchers.allOf
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

class MainActivityTest{
    @Rule
    @JvmField
    var activity = ActivityScenarioRule(MainActivity::class.java)

    @Rule
    @JvmField
    var intents = IntentsTestRule(MainActivity::class.java)

    @Test
    fun testShowUser(){
        Espresso.onView(withId(R.id.edt_user)).check(matches(isDisplayed()))
    }

    @Test
    fun testShowPass(){
        Espresso.onView(withId(R.id.edt_pass)).check(matches(isDisplayed()))
    }

    @Test
    fun testShowButtonLogin(){
        Espresso.onView(withId(R.id.btn_login)).check(matches(isDisplayed()))
    }

    @Test
    fun testLogin(){
        Espresso.onView(withId(R.id.edt_user)).perform(typeText("abcdef"))
        Espresso.onView(withId(R.id.edt_pass)).perform(ViewActions.typeText("123456"))

        Espresso.onView(withId(R.id.btn_login)).perform(ViewActions.click())
        Espresso.onView(withId(R.id.tv_title)).check(ViewAssertions.matches(isDisplayed()))
    }

    @Test
    fun testSelectContact(){
//        var result = Instrumentation.ActivityResult(
//            Activity.RESULT_OK, Intent(null,
//            ContactsContract.Contacts.CONTENT_URI)
//        )
//        Intents.intending(hasAction(Intent.ACTION_PICK)).respondWith(result)
//        Espresso.onView(withId(R.id.btn_contact)).perform(ViewActions.click())
//        Intents.intended(allOf(
//            toPackage("com.google.android.contacts"),
//            hasAction(Intent.ACTION_PICK),
//            hasData(ContactsContract.Contacts.CONTENT_URI)))
    }

    @Test
    fun testIntentUrl(){
        Espresso.onView(withId(R.id.btn_start)).check(matches(isDisplayed()))
        Espresso.onView(withId(R.id.btn_start)).check(matches(withText("Start new")))
        Espresso.onView(withId(R.id.btn_start)).perform(ViewActions.click())
        intended(hasExtra("URL", "haobk4dev.wordpress.com"))
    }
}