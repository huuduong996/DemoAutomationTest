package com.ringme.automationtest

import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

class SecondActivityTest{
    @Rule
    @JvmField
    var secondActivity = ActivityScenarioRule(SecondActivity::class.java)
    @Test
    fun testShowTitle(){
        Espresso.onView(withId(R.id.tv_title)).check(matches(isDisplayed()))
    }
}