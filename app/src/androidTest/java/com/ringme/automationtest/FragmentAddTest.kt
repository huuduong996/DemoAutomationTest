package com.ringme.automationtest

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class FragmentAddTest{
    private lateinit var scenario: FragmentScenario<FragmentAdd>

    @Before
    fun setup() {
        scenario = launchFragmentInContainer(themeResId = R.style.Theme_AutomationTest)
        scenario.moveToState(Lifecycle.State.STARTED)
    }

    @Test
    fun testAddingSpend() {
        val amount = 100
        val desc = "Bought Eggs"
        //Espresso Matcher and Action
        Espresso.onView(withId(R.id.edt_amount)).perform(typeText(amount.toString()))
        Espresso.onView(withId(R.id.edt_desc)).perform(typeText(desc))
        Espresso.closeSoftKeyboard()
        Espresso.onView(withId(R.id.btn_add)).perform(ViewActions.click())

        //Assertion
        Espresso.onView(withId(R.id.tv_success)).check(matches(withText("Spend Added")))
    }

}